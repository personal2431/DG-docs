import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy import text

class EDA_AI_Utils:
    def __init__(self,engine, server_name="XXXXXXXX", database="DRAFT") -> None:
        """init EDA_AI_Utils object.
        Some additional authentication needs to be done when initialize the object
        """
        self.server_name = server_name
        self.database = database
        # self.engine = create_engine(
        #     "mssql+pyodbc://@"
        #     + server_name
        #     + "/"
        #     + database
        #     + "?trusted_connection=yes&driver=ODBC+Driver+17+for+SQL+Server", fast_executemany=True, future=True
        # )
        self.engine = engine
    def execute_query(self, query:str): 
        with self.engine.connect() as connection:
            connection.execute(text(query))
            connection.commit()

    def query_data(self, query: str) -> object:
        """query data in the DW backend, return a pandas dataframe

        Args:
            query (string): sql query
        Return: dataframe

        """
        with self.engine.begin() as conn:
            df = pd.read_sql_query(text(query), conn)
        return df

    def push_table(self, df: pd.DataFrame, table_name: str) -> None:
        """
        Upload data and replace if table exist
        Args:
            df: dataframe to be uploaded
            table_name:

        Returns:

        """
        df.to_sql(table_name, self.engine, if_exists="append", index=False)
