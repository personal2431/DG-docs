import datetime
import pandas as pd
import random
from database import EDA_AI_Utils
from sqlalchemy import create_engine
# from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine import URL
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.sql import func

from sqlalchemy import DateTime,Column,ForeignKey,BigInteger,String,Date,Numeric,Integer
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import relationship
Base = declarative_base()
DBSession = scoped_session(sessionmaker())
engine = None


# engine= create_engine("mssql+pyodbc://sa:123456789aA@@eks-poc-nlb-1f298278e532528a.elb.ap-southeast-1.amazonaws.com")
class Customer(Base):
    __tablename__ = "CUSTOMER"
    
    
    CUST_ANCHOR_ID = Column(BigInteger, primary_key=True,autoincrement=False)
    BUSINESS_DATE = Column(Date)
    PPN_DATE = Column(String(24))
    CUST_CODE = Column(String(64))
    CUST_NAME = Column(String(256))
    VALUE_DATE = Column(Date)
    ACTUAL_END_DATE = Column(Date)
    SRC_SYS_NAME = Column(String(64))
    SRC_SYS_CODE = Column(String(64))
    UNIQ_ID_IN_SRC_SYS = Column(String(256))
    # RM_DAO_ANCHOR_ID :Column(BigInteger)
    RM_DAO_CODE = Column(String(64))
    # SECTOR_ANCHOR_ID :Column(BigInteger)
    SECTOR_CODE = Column(String(10))
    VIP_TYPE_CODE = Column(String(64))
    VIP_TYPE_NAME = Column(String(100))
    SEG_CODE = Column(String(10))
    SEG_NAME = Column(String(24))
    CUST_TYPE_NAME = Column(String(64))
    CUST_TYPE_CODE = Column(String(32))
    OCB_CUST_FLAG = Column(String(64))
    LEGAL_ID = Column(String(32))
    SECTOR_NAME = Column(String(64))
    PREM_SERVICE_CUST_FLAG = Column(String(10))
    # INDUSTRY_ANCHOR_ID :Column(BigInteger)
    INDUSTRY_CODE = Column(String(10))
    # MGMT_BRANCH_ANCHOR_ID :Column(BigInteger)
    MGMT_BRANCH_CODE = Column(String(32))
    DATE_OF_BIRTH = Column(Date)
    NATIONLT_CODE = Column(String(64))
    SWIFT_CODE = Column(String(32))
    RESIDEN_CODE = Column(String(64))
    TAX_NBR = Column(String(50))
    TOTAL_CAPITAL_OWNER = Column(String(32))
    NAME_OF_OFFICE = Column(String(255))
    # NBR_OF_EMP :Column(BigInteger)
    GENDER = Column(String(64))
    MARITAL_STATUS = Column(String(64))
    CUST_OPEN_DATE = Column(DateTime(timezone=True), default=func.now())
    REV = Column(String(20))
    NBR_OF_EMP_PAYING_SOCIAL_INSRC = Column(String(20))
    LAST_UPD_DATE = Column(DateTime(timezone=True), default=func.now())
    AUTH = Column(String(40))
    CUST_SHORT_NAME = Column(String(256))
    CUST_ALT_NAME = Column(String(256))
    CUST_ALT_SHORT_NAME = Column(String(256))
    RESIDEN_OF_PARENT = Column(String(64))
    VPB_CHANNEL = Column(String(24))
    RESIDEN_DIST_CODE = Column(String(20))
    RESIDEN_PROVIN_CODE = Column(String(20))
    RESIDEN_WARD_CODE = Column(String(20))
    RESIDEN_PROVIN_NAME = Column(String(64))
    CUST_ASSET_CLASS = Column(String(10))
    # DAO_ANCHOR_ID :Column(BigInteger)
    DAO_CODE = Column(String(64))
    # PRIORITY_BRANCH_ANCHOR_ID :Column(BigInteger)
    PRIORITY_BRANCH_CODE = Column(String(64))
    INTRO_CODE = Column(String(64))
    INCOME_SEG = Column(String(64))
    TITLE = Column(String(64))
    TC_SIGN_DATE = Column(Date)
    VIP_CHANGE_DATE = Column(Date)
    TAX_CODE_ISSU_DATE = Column(Date)
    VIP_GROUP = Column(String(64))
    # STATUS_ANCHOR_ID :Column(BigInteger)
    STATUS_CODE = Column(String(64))
    VIP_INFO = Column(String(110))
    NETWRTH_INFO = Column(String(24))
    PRD_UPG_AF_CUST = Column(String(24))
    AF_CHANGE_REASON = Column(String(255))
    PAYROL_DAO = Column(String(24))
    INPT_ANCHOR_ID :Column(BigInteger)
    INPT_CODE = Column(String(24))
    EMAIL = Column(String(110))
    EDU = Column(String(110))
    CHANNEL = Column(String(64))
    PROM_CODE = Column(String(256))
    PARTNER_CODE = Column(String(64))
    SME_OWNER_CODE = Column(String(24))
    VPB_SERVICE = Column(String(32))
    # AUTH_ANCHOR_ID :Column(BigInteger)
    VIP_CODE = Column(String(32))
    PROF = Column(String(255))
    INSRC_FLAG = Column(String(10))
    FDI_SEG = Column(String(24))
    ACCT_OFFICER = Column(String(24))
    # NATIONLT_ANCHOR_ID :Column(BigInteger)
    # RESIDEN_ANCHOR_ID :Column(BigInteger)
    # DAO_MGMT_ANCHOR_ID :Column(BigInteger)
    DAO_MGMT_CODE = Column(String(64))
    AF_ID_CHANNEL = Column(String(32))
    AF_INFO = Column(String(256))
    AF_TC_SIGN_DATE = Column(Date)
    MAF_OUT_REASON = Column(String(256))
    FULL_NAME_VN = Column(String(356))

    # addresses = relationship(
    #     "Contact", back_populates="customer", cascade="all, delete-orphan"
    # )
    def __repr__(self):
        return f"User(id={self.CUST_CODE!r}, name={self.CUST_NAME!r})"

class Contact(Base):
    __tablename__ = "CUSTOMER_CONTACT"
    ID = Column(BigInteger, primary_key=True,autoincrement=True)
    CUST_ID = Column(BigInteger, ForeignKey("CUSTOMER.CUST_ANCHOR_ID"), nullable=False)
    CONTACT_NUM = Column(String, nullable=False)
    # user = relationship("Customer", back_populates="contact")
    def __repr__(self):
        return f"Address(id={self.ID!r}, contact={self.CONTACT_NUM!r})"

# class FCY (Base):
#     ID = Column(BigInteger, primary_key=True,autoincrement=True)
#     YEARMONTH = Column(String(6))
#     APP = Column(String(20))
#     ACCTNO = Column(String(20))
#     CURRENRCY = Column(String(3))
#     N01 :None,
#     N02 :None,
#     N03 :None,
#     N04 :None,
#     N05 :None,
#     N06 :None,
#     N07 :None,
#     N08 :None,
#     N09 :None,
#     N10 :None,
#     N11 :None,
#     N12 :None,
#     N13 :None,
#     N14 :None,
#     N15 :None,
#     N16 :None,
#     N17 :None,
#     N18 :None,
#     N19 :None,
#     N20 :None,
#     N21 :None,
#     N22 :None,
#     N23 :None,
#     N24 :None,
#     N25 :None,
#     N26 :None,
#     N27 :None,
#     N28 :None,
#     N29 :None,
#     N30 :None,
#     N31 :None,
#     NUMBER_OF_DAY = Column(BigInteger)
#     ACCRUAL_BAL = Column(Integer)
#     AVERAGE_BAL :None,
#     CIF = Column(String(20))
#     BI_SEGMENT = Column(String(10))
   
def init_sqlalchemy():
    global engine
    connection_string = "DRIVER={ODBC Driver 17 for SQL Server};SERVER=k8s-openmeta-nlbmssql-7e67da64a7-d8f9f9441a4d9977.elb.ap-southeast-1.amazonaws.com;DATABASE=master;UID=sa;PWD=123456789aA@;fast_executemany=True"
    connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})
    engine = create_engine(connection_url)
    DBSession.remove()
    DBSession.configure(bind=engine, autoflush=False, expire_on_commit=False)
    # Base.metadata.drop_all(engine)
    # Base.metadata.create_all(engine)

def create_customer_data(n):
    data=[]
    for i in range (n,n+10000):
        d = {
                "CUST_ANCHOR_ID":i,
                "BUSINESS_DATE":'2023-12-19',
                "PPN_DATE":'2023-12-19',
                "CUST_CODE":str(i),
                "CUST_NAME": "Nguyen Van A"+str(i),
                "VALUE_DATE":'2023-12-19',
                "ACTUAL_END_DATE":'2023-12-19',
                "SRC_SYS_NAME" :"VPBANK",
                "SRC_SYS_CODE" :"VPB",
                "UNIQ_ID_IN_SRC_SYS":"XXXXYYYYY",
                # "RM_DAO_ANCHOR_ID":i,
                "RM_DAO_CODE":str(i),
                # "SECTOR_ANCHOR_ID" :i,
                "SECTOR_CODE":str(i),
                "VIP_TYPE_CODE":"VIP"+str(i),
                "VIP_TYPE_NAME":"VIPPPP",
                "SEG_CODE":"SEG_CODE",
                "SEG_NAME":"SEG_NAME",
                "CUST_TYPE_NAME":"CUST_TYPE_NAME",
                "CUST_TYPE_CODE":"CUST_TYPE_CODE",
                "OCB_CUST_FLAG":"VN",
                "LEGAL_ID":"LEGAL_ID"+str(i),
                "SECTOR_NAME" :"SECTOR_NAME",
                "PREM_SERVICE_CUST_FLAG":"TEST PREM",
                # "INDUSTRY_ANCHOR_ID" :i,
                "INDUSTRY_CODE":"10101",
                # "MGMT_BRANCH_ANCHOR_ID" :i,
                "MGMT_BRANCH_CODE":str(i),
                "DATE_OF_BIRTH":'2023-12-19',
                "NATIONLT_CODE":"VN",
                "SWIFT_CODE":"100000",
                "RESIDEN_CODE":"VN",
                "TAX_NBR":"1000",
                "TOTAL_CAPITAL_OWNER":"dadadadad",
                "NAME_OF_OFFICE":"brancsda",
                # "NBR_OF_EMP" :i,
                "GENDER":"MALE",
                "MARITAL_STATUS":"YES",
                "CUST_OPEN_DATE":datetime.datetime.now(),
                "REV":"REV",
                "NBR_OF_EMP_PAYING_SOCIAL_INSRC":'TEST NBR PAYING',
                "LAST_UPD_DATE":datetime.datetime.now(),
                "AUTH":"ADMIN",
                "CUST_SHORT_NAME":"NGUYEN VAN A"+ str(i),
                "CUST_ALT_NAME" :"NGUYEN VAN A"+ str(i),
                "CUST_ALT_SHORT_NAME": "NGUYEN VAN A"+ str(i),
                "RESIDEN_OF_PARENT":"PArent",
                "VPB_CHANNEL" :"KHCN",
                "RESIDEN_DIST_CODE":"VN",
                "RESIDEN_PROVIN_CODE":"HCM",
                "RESIDEN_WARD_CODE":"GV",
                "RESIDEN_PROVIN_NAME" :"Ho Chi Minh",
                "CUST_ASSET_CLASS":"CUSt TEST",
                # "DAO_ANCHOR_ID" :i,
                "DAO_CODE":"DAO CODE"+str(i),
                # "PRIORITY_BRANCH_ANCHOR_ID" :i,
                "PRIORITY_BRANCH_CODE":str(i),
                "INTRO_CODE":"INTRO_CODE",
                "INCOME_SEG":"INCOME_SEG",
                "TITLE":"VOP",
                "TC_SIGN_DATE":'2023-12-19',
                "VIP_CHANGE_DATE" :'2023-12-19',
                "TAX_CODE_ISSU_DATE" :'2023-12-19',
                "VIP_GROUP":"VIP",
                # "STATUS_ANCHOR_ID" :i,
                "STATUS_CODE" :"1",
                "VIP_INFO" :"VIP",
                "NETWRTH_INFO":"NETWRTH_INFO",
                "PRD_UPG_AF_CUST":"PRD_UPG_AF_CUST",
                "AF_CHANGE_REASON":None,
                "PAYROL_DAO" :None,
                # "INPT_ANCHOR_ID" :i,
                "INPT_CODE" :str(i),
                "EMAIL" :str(i)+"@gmail.com",
                "EDU":"DH",
                "CHANNEL":"dadasdasda",
                "PROM_CODE":None,
                "PARTNER_CODE" :None,
                "SME_OWNER_CODE" :None,
                "VPB_SERVICE" :None,
                # "AUTH_ANCHOR_ID" :i,
                "VIP_CODE":"VIP",
                "PROF":"PROP",
                "INSRC_FLAG" :"VN",
                "FDI_SEG":"FDI",
                "ACCT_OFFICER":"VPBBBBB",
                # "NATIONLT_ANCHOR_ID" :i,
                # "RESIDEN_ANCHOR_ID" :i,
                # "DAO_MGMT_ANCHOR_ID" :i,
                "DAO_MGMT_CODE":"CODE"+str(i),
                "AF_ID_CHANNEL":"test",
                "AF_INFO":"AF",
                "AF_TC_SIGN_DATE":"2023-12-19",
                "MAF_OUT_REASON" :None,
                "FULL_NAME_VN":"NGUYEN VAN A"+ str(i)
            }
        data.append(d)
    return data
def create_data_customer_contact(n):
    data=[]
    for i in range(n,n+10000):
       d= {
            "CUST_ID":i,
            "CONTACT_NUM":str(i)
       }
       data.append(d)
    return data
def create_data_fcy():
    data=[]
    for i in range(1,10000):
        d={
            "YEARMONTH":'202312',
            "APP":'DEPOSIT',
            "ACCTNO":str(random.randint(100000,999999)),
            "N01" :0.00,
            "N02" :0.01,
            "N03" :0.02,
            "N04" :0.03,
            "N05" :0.04,
            "N06" :0.05,
            "N07" :0.06,
            "N08" :0.07,
            "N09" :None,
            "N10" :None,
            "N11" :None,
            "N12" :None,
            "N13" :None,
            "N14" :None,
            "N15" :None,
            "N16" :None,
            "N17" :None,
            "N18" :None,
            "N19" :None,
            "N20" :None,
            "N21" :None,
            "N22" :None,
            "N23" :None,
            "N24" :None,
            "N25" :None,
            "N26" :None,
            "N27" :None,
            "N28" :None,
            "N29" :None,
            "N30" :None,
            "N31" :None,
            "NUMBER_OF_DAY":8,
            "ACCRUAL_BAL":0,
            "AVERAGE_BAL" :0.00,
            "CIF":str(random.randint(100000,999999)),
            "BI_SEGMENT":"BI"
        }
    return data
     
def main():
    # for i in range(4,27): #3
    init_sqlalchemy()
    # db= EDA_AI_Utils(engine)
    n= 2*10000
    data = create_data_customer_contact(n)
    t= datetime.datetime.now()
    # # create_data_customer_contact()
    engine.execute(
        Contact.__table__.insert(),data
    )
    # # print("turn ",i)
    # db.push_table(pd.DataFrame(data),"CUSTOMER")
    print(datetime.datetime.now()-t)
main()